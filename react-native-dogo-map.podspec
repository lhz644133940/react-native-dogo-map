require 'json'

package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name         = "react-native-dogo-map"
  s.version      = package['version']
  s.summary      = "React Native Mapview component for iOS + Android"

  s.authors      = { "LHZ" => "lhz644133940@qq.com" }
  s.homepage     = "https://gitlab.com/lhz644133940/react-native-dogo-map"
  s.license      = "MIT"
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitlab.com/lhz644133940/react-native-dogo-map.git", :tag=> "v#{s.version}" }
  s.source_files  = "lib/ios/AirMaps/**/*.{h,m}"

  s.dependency 'React'
end
